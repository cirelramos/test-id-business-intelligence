<?php

namespace App\Http\Controllers\User;

use App\Core\User\Requests\StoreUserRequest;
use App\Core\User\Services\UserService;
use App\Http\Controllers\ApiController;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends ApiController
{

    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct( UserService $userService )
    {

        $this->middleware( 'auth:api' )->except( [ 'store' ] );
        $this->userService = $userService;
    }

    public function index( Request $request )
    {

        return $request;
    }

    /**
     * @param StoreUserRequest $storeUserRequest
     * @return JsonResponse|null
     * @throws Exception
     */
    public function store( StoreUserRequest $storeUserRequest ): ?JsonResponse
    {

        $errorMessage   = 'error al crear el usuario';
        $successMessage = 'usuario creado exitosamente';

        try {

            DB::beginTransaction();

            $user = $this->userService->save( $storeUserRequest );
            DB::commit();

            return $this->showMessage( $successMessage, 201, $user );
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse( $errorMessage, 500 );
        }

    }

}
