<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;

/**
 * Trait FileStorageS3
 * @package App\Traits
 */
trait  FileStorageS3
{

    public $pathProfile = 'img/picture_profile/';

    public $storage;

    /**
     * @return mixed
     */
    public function getS3Url()
    {

        return env( 'AWS_URL_BUCKET', '' );
    }

    /**
     * @param $imgPath
     * @return mixed
     */
    public function getImageProfile( $imgPath )
    {

        if ( $imgPath ) {

            if ( $this->validFile( $imgPath ) ) {
                return Storage::disk( 's3' )->url( $imgPath );
            }

        }

        $this->getImageProfileDefault();

    }

    /**
     * @param $path
     * @return bool
     */
    private function validFile( $path ): bool
    {

        $response = false;
        if ( Storage::disk( 's3' )->exists( $path ) && Storage::disk( 's3' )->getVisibility( $path ) === 'public' ) {
            $response = true;
        }

        return $response;
    }

    public function getImageProfileDefault()
    {

        $random     = rand( 1, 4 );
        $imgDefault = $this->pathProfile . "default_$random.jpg";

        return Storage::disk( 's3' )->url( $imgDefault );
    }

    public function getUrlFileS3( $path = '' )
    {

        if ( $path !== '' ) {
            return Storage::disk( 's3' )->temporaryUrl( $path, now()->addMinutes( 10 ) );
        }

        return $path;
    }

    /**
     * @param      $path
     * @param bool $version (Si la imagen quieres que retorna un numero version por cambio)
     * @return string
     */
    protected function existImageS3( $path, $version = true ): string
    {

        $exits = Storage::disk( 's3' )->exists( $path );

        if ( $exits ) {
            $response = Storage::disk( 's3' )->url( $path ) . ( ( $version === true ) ? '?=' . str_random() : '' );
        } else {
            $response = '';
        }

        return $response;
    }

    /**
     * @param        $uploadedFile
     * @param        $directory
     * @param string $nameFile
     * @return mixed
     */
    protected function saveDefaultImg( $uploadedFile, $directory, $nameFile = '' )
    {

        $getNameFileUpload    = $uploadedFile->getClientOriginalName();
        $getOriginalExtension = $uploadedFile->getClientOriginalExtension();
        $fileName             = pathinfo( $getNameFileUpload, PATHINFO_FILENAME );

        $s3       = Storage::disk( 's3' );
        $filePath = $directory . ( empty( $nameFile ) ? str_slug( $fileName )
                : str_slug( $nameFile ) ) . '.' . $getOriginalExtension;
        $s3->put( $filePath, file_get_contents( $uploadedFile ), 'public' );

        $message = Lang::getFromJson( 'Imported file' );

        return $this->successResponse( [ 'success' => true, 'message' => $message, 'url' => $s3->url( $filePath ) ] );
    }

    /**
     * @param        $file
     * @param        $path
     * @param string $type Tipo de archivo (public or private)
     * @return mixed
     */
    protected function uploadFileS3( $file, $path, $type )
    {

        $getNameFileUpload    = $file->getClientOriginalName();
        $getOriginalExtension = $file->getClientOriginalExtension();
        $fileName             = pathinfo( $getNameFileUpload, PATHINFO_FILENAME );
        $name                 = str_slug( $fileName . str_random( 10 ) ) . '.' . $getOriginalExtension;

        $s3       = Storage::disk( 's3' );
        $filePath = $path . $name;
        $s3->put( $filePath, file_get_contents( $file ), $type );

        $message = Lang::getFromJson( 'Imported file' );

        return $this->successResponse( [ 'success' => true, 'message' => $message, 'url' => $s3->url( $filePath ) ] );
    }

    /**
     * @param        $file
     * @param        $path
     * @param int    $minute
     * @return mixed
     */
    protected function uploadFileS3Temp( $file, $path, int $minute )
    {

        $getNameFileUpload    = $file->getClientOriginalName();
        $getOriginalExtension = $file->getClientOriginalExtension();
        $fileName             = pathinfo( $getNameFileUpload, PATHINFO_FILENAME );
        $name                 = str_slug( $fileName . str_random( 10 ) ) . '.' . $getOriginalExtension;

        $s3       = Storage::disk( 's3' );
        $filePath = $path . $name;
        $s3->put( $filePath, file_get_contents( $file ), 'private' );

        $url = $s3->temporaryUrl( $filePath, now()->addMinutes( $minute ) );

        $message = Lang::getFromJson( 'Imported file' );

        return $this->successResponse( [ 'success' => true, 'message' => $message, 'url' => $url ] );
    }

    /**
     * @param     $pathFileToS3
     * @param     $saveDirectory
     * @param int $time
     * @return mixed
     */
    protected function moveFileLocalToS3( $pathFileToS3, $saveDirectory, $time = 10 )
    {

        $diskS3 = Storage::disk( 's3' );
        $diskS3->put( $pathFileToS3, fopen( $saveDirectory, 'rb+' ) );

        return $diskS3->temporaryUrl( $pathFileToS3, now()->addMinutes( $time ) );
    }

    /**
     * @param $pathFileToS3
     * @param $saveDirectory
     * @return mixed
     */
    protected function moveFileLocalToS3NoTemp( $pathFileToS3, $saveDirectory )
    {

        $diskS3 = Storage::disk( 's3' );
        $diskS3->put( $pathFileToS3, fopen( $saveDirectory, 'rb+' ) );

        return $diskS3->url( $pathFileToS3 );
    }

    protected function moveStreamS3( $pathFileToS3, $saveDirectory )
    {

        $diskS3 = Storage::disk( 's3' );
        $diskS3->put( $pathFileToS3, $saveDirectory );

        return $diskS3->temporaryUrl( $pathFileToS3, now()->addMinutes( 10 ) );
    }

    /**
     * @param $saveDirectory
     */
    protected function deleteFileLocal( $saveDirectory ): void
    {

        if ( file_exists( $saveDirectory ) ) {
            unlink( $saveDirectory );
        }
    }

    protected function retrieveS3fileContent( $pathFileToS3 )
    {

        $diskS3 = Storage::disk( 's3' );

        return $diskS3->get( $pathFileToS3 );
    }


    protected function uploadPhoto( $uploadedFile, $pathDirectory)
    {
        // Disco seleccionado
        $s3 = Storage::disk(self::DISK);

        /** Crear el directorio de mediciones por compañia **/
        $directory = 'test' . '/' . $pathDirectory . '/';

        /** Nombre del archivo subido **/
        $dateUpload = Carbon::now(self::TIME_ZONE)->toDateTimeString();
        $name       = str_slug('logo' . '-' . $dateUpload . ' - ' . md5_file($uploadedFile)) . self::EXTENSION;

        /** Guarda en el directorio especificado **/
        $filePath = self::DIRECTORY_FILE . $directory . $name;

        $img = null;
        if ( !empty($uploadedFile)) {
            $s3->put($filePath, file_get_contents($uploadedFile), self::TYPE_OPTION);
            $img = $s3->url($filePath) . '?=' . str_random();
        }

        return $img;
    }
}
