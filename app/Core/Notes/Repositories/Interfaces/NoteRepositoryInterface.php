<?php

namespace App\Core\Notes\Repositories\Interfaces;

use App\Core\Notes\Models\Note;

/**
 * Interface NoteRepositoryInterface
 * @package App\Core\Notes\Repositories\Interfaces
 */
interface NoteRepositoryInterface
{

    /**
     * @return Note
     */
    public function getModel(): Note;

    /**
     * @param Note $note
     * @return mixed
     */
    public function save( Note $note );

    /**
     * @param Note $note
     * @return mixed
     */
    public function updateNote( Note $note );

    /**
     * @param Note $note
     * @param      $idImage
     * @return mixed
     */
    public function saveNoteHasImage( Note $note, $idImage );

    /**
     * @param $weekStartDate
     * @param $weekEndDate
     * @return mixed
     */
    public function listNotesByWeekQuery( $weekStartDate, $weekEndDate );
}
