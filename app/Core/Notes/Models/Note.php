<?php

namespace App\Core\Notes\Models;

use App\Core\Groups\Models\Group;
use App\Core\Images\Models\Image;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Core\Notes\Models\Note
 *
 * @property varchar                                  $name        name
 * @property varchar                                  $description description
 * @property timestamp                                $created_at  created at
 * @property timestamp                                $updated_at  updated at
 * @property timestamp                                $deleted_at  deleted at
 * @property Collection $groupshass  belongsToMany
 * @property Collection $shasimage   belongsToMany
 * @property int $id_note
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Groups\Models\Group[] $groups
 * @property-read int|null $groups_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Images\Models\Image[] $images
 * @property-read int|null $images_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note whereIdNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\Note whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Notes\Models\Note onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Notes\Models\Note withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Notes\Models\Note withoutTrashed()
 */
class Note extends Model
{

    use SoftDeletes;
    /**
     * Database table name
     */
    protected $table = 'notes';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_note';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * groupshasses
     *
     * @return BelongsToMany
     */
    public function groups()
    {

        return $this->belongsToMany( Group::class, 'groups_has_notes' );
    }

    /**
     * shasimages
     *
     * @return BelongsToMany
     */
    public function images()
    {

        return $this->belongsToMany( Image::class, 'notes_has_images', 'id_note', 'id_image' )
            ->whereNull( 'images.deleted_at' )
            ;
    }

}
