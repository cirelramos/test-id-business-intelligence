<?php

namespace App\Core\User\Services;

use App\Core\User\Models\User;
use App\Core\User\Repositories\interfaces\UserRepositoryInterface;
use App\Core\User\Requests\StoreUserRequest;

/**
 * Class UserService
 * @package App\Core\User\Services
 */
class UserService
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct( UserRepositoryInterface $userRepository )
    {

        $this->userRepository = $userRepository;
    }

    /**
     * @param $storeUserRequest
     * @return User
     */
    public function save( StoreUserRequest $storeUserRequest ): User
    {

        $user = $this->userRepository->getModel();
        $input = $storeUserRequest->all();
        $input['password'] = bcrypt($input['password']);
        $user->fill( $input );

        $user = $this->userRepository->save( $user );

        $user->token =  $user->createToken('Personal Access Token');

        return $user;


    }

}
