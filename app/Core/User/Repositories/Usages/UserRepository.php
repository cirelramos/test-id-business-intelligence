<?php

namespace App\Core\User\Repositories\Usages;

use App\Core\Base\Repositories\Usages\BaseRepository;
use App\Core\User\Models\User;
use App\Core\User\Repositories\interfaces\UserRepositoryInterface;

/**
 * Class UserRepository
 * @package App\Core\User\Repositories\Usages
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * @var User
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param User $model
     */
    public function __construct( User $model )
    {

        $this->model = $model;
        parent::__construct( $model );
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function getAllUserByIdsQuery( array $ids )
    {

        return $this->getModel()->whereIn( 'id', $ids );

    }

    /**
     * @return User
     */
    public function getModel(): User
    {

        return $this->model;
    }

    /**
     * @param User $user
     * @return User
     */
    public function save( User $user ): User
    {

        $user->save();

        return $user;

    }

}
