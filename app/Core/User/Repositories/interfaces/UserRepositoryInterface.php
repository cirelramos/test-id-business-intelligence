<?php

namespace App\Core\User\Repositories\interfaces;

use App\Core\User\Models\User;

/**
 * Class UserRepositoryInterface
 * @package App\Core\User\Repositories\interfaces
 */
interface UserRepositoryInterface
{

    /**
     * @param array $ids
     * @return mixed
     */
    public function getAllUserByIdsQuery( array $ids );
    /**
     * @return User
     */
    public function getModel(): User;

    /**
     * @param User $user
     * @return User
     */
    public function save( User $user ): User;

}
