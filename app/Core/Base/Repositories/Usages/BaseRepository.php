<?php

namespace App\Core\Base\Repositories\Usages;

use App\Core\Base\Repositories\Interfaces\BaseRepositoryInterface;
use App\Traits\BulkModel\BulkModel;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class BaseRepository
 *
 * @package App\HR\Base\Repositories
 */
class BaseRepository implements BaseRepositoryInterface
{

    use BulkModel;

    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct( Model $model )
    {

        $this->model = $model;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create( array $attributes )
    {

        return $this->model::create( $attributes );
    }

    /**
     * @param array $data
     * @return bool
     */
    public function update( array $data ): bool
    {

        return $this->model->update( $data );
    }

    /**
     * @param array  $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all( $columns = [ '*' ], string $orderBy = 'id', string $sortBy = 'asc' )
    {

        return $this->model->orderBy( $orderBy, $sortBy )->get( $columns );
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function find( $id )
    {

        return $this->model->find( $id );
    }

    /**
     * @param  $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneOrFail( $id )
    {

        return $this->model::findOrFail( $id );
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function findBy( array $data )
    {

        return $this->model->where( $data )->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function findOneBy( array $data )
    {

        return $this->model->where( $data )->first();
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneByOrFail( array $data )
    {

        return $this->model->where( $data )::firstOrFail();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function delete(): bool
    {

        return $this->model->delete();
    }

    /**
     * @param Model       $table
     * @param array       $values
     * @param string|null $index
     * @return bool
     */
    public function updateBulk( Model $table, array $values, string $index = null ): bool
    {

        return $this->updateBatch( $table, $values, $index );
    }
}
