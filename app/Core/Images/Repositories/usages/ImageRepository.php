<?php

namespace App\Core\Images\Repositories\usages;

use App\Core\Base\Repositories\Usages\BaseRepository;
use App\Core\Images\Models\Image;
use App\Core\Images\Repositories\interfaces\ImageRepositoryInterface;
use App\Core\Notes\Models\Note;
use App\Core\Notes\Models\NotesHasImage;
use Illuminate\Database\Query\Builder;

/**
 * Class ImageRepository
 * @package App\Core\Images\Repositories\usages
 */
class ImageRepository extends BaseRepository implements ImageRepositoryInterface
{

    /**
     * @var Image
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Image $model
     */
    public function __construct( Image $model )
    {

        $this->model = $model;
        parent::__construct( $model );
    }

    /**
     * @param      $arrayIdImage
     * @param Note $note
     * @return NotesHasImage|\Illuminate\Database\Eloquent\Builder|Builder
     */
    public function getNoteHasImagesByArrayImageAndNoteQuery( $arrayIdImage, Note $note )
    {

        return $this->getModelNoteHasImage()::whereNotIn( 'id_image', $arrayIdImage )
            ->where( 'id_note', $note->id_note )
            ;

    }

    /**
     * @return NotesHasImage
     */
    public function getModelNoteHasImage(): NotesHasImage
    {

        return new NotesHasImage();
    }

    /**
     * @param Image $image
     * @return Image
     */
    public function save( Image $image )
    {

        $image->save();

        return $image;
    }

    /**
     * @param $arrayIdImage
     * @return mixed
     */
    public function deleteImageByArrayImage( $arrayIdImage )
    {

        $deleteNoteHasImage = $this->getModelNoteHasImage()::whereIn( 'id_image', $arrayIdImage )->delete();

        $deleteImage = $this->getModel()::whereIn( 'id_image', $arrayIdImage )->delete();

        return $deleteImage && $deleteNoteHasImage;

    }

    /**
     * @return Image
     */
    public function getModel(): Image
    {

        return $this->model;
    }

}
