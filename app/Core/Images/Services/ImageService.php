<?php

namespace App\Core\Images\Services;

use App\Core\Images\Models\Image;
use App\Core\Images\Repositories\interfaces\ImageRepositoryInterface;
use App\Core\Notes\Models\Note;
use App\Traits\FileStorageS3;
use Exception;

/**
 * Class ImageService
 * @package App\Core\Images\Services
 */
class ImageService
{

    use FileStorageS3;

    public const DISK           = 's3';

    public const DIRECTORY_FILE = 'img/';

    public const EXTENSION      = '.jpg';

    public const TYPE_OPTION    = 'public';

    public const IMG_HEIGHT     = 700;

    public const IMG_WITH       = 700;

    public const TIME_ZONE      = 'America/Lima';

    /**
     * @var ImageRepositoryInterface
     */
    private $imageRepository;

    /**
     * ImageService constructor.
     * @param ImageRepositoryInterface $imageRepository
     */
    public function __construct( ImageRepositoryInterface $imageRepository )
    {

        $this->imageRepository = $imageRepository;
    }

    public function save( $storeNoteRequests )
    {

        $images = $storeNoteRequests->file( 'images' );
        $images = collect( $images );

        $images = $images->map( $this->mapUploadImageTransform() );

        $images = $images->map( $this->mapSaveImageTransform() );

        return $images;

    }

    /**
     * @return callable
     */
    private function mapUploadImageTransform(): callable
    {

        return function ( $item, $key ) {

            return [ 'path' => $this->uploadImage( $item[ 'image' ] ) ];

        };
    }

    /**
     * @param $uploadFile
     * @return string|null
     */
    public function uploadImage( $uploadFile )
    {

        $pathDirectory = 'profile-picture';

        return $this->uploadPhoto( $uploadFile, $pathDirectory );

    }

    private function mapSaveImageTransform(): callable
    {

        return function ( $item, $key ) {

            $image = new Image();
            $image->fill( $item );

            return $this->imageRepository->save( $image );
        };
    }

    /**
     * @param      $updateNoteRequests
     * @param Note $note
     * @throws Exception
     */
    public function deleteImagesRelationWithNotes( $updateNoteRequests, Note $note ): void
    {

        $arrayIdImage = collect( $updateNoteRequests->images )
            ->where( 'id_image', '!=', null )
            ->pluck( 'id_image' )
            ->toArray()
        ;

        if(count($arrayIdImage)>0){
            $arrayIdImage = $this->imageRepository->getNoteHasImagesByArrayImageAndNoteQuery( $arrayIdImage, $note )
                ->get( [ 'id_image' ] )
                ->toArray()
            ;
            $this->imageRepository->deleteImageByArrayImage( $arrayIdImage );
        }

    }

}
