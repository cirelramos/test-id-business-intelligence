<?php

namespace App\Core\Groups\Services;

use App\Core\Groups\Models\Group;
use App\Core\Groups\Models\GroupsHasUser;
use App\Core\Groups\Repositories\Interfaces\GroupRepositoryInterface;
use App\Core\Notes\Models\Note;
use App\Core\Notes\Notifications\NoteStoreNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GroupService
 * @package App\Core\Groups\Services
 */
class GroupService
{

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * GroupService constructor.
     * @param GroupRepositoryInterface $groupRepository
     */
    public function __construct( GroupRepositoryInterface $groupRepository )
    {

        $this->groupRepository = $groupRepository;
    }

    /**
     * @return Group[]|Collection
     */
    public function listGroups()
    {

        return $this->groupRepository->getModel()::with( 'users' )->get();
    }

    /**
     * @param Group $group
     * @return Builder|Model|object
     */
    public function show( Group $group )
    {

        return $this->groupRepository->getModel()::where( 'id_group', $group->id_group )
            ->with( 'users', 'notes.images' )
            ->first()
            ;
    }

    /**
     * @param Group $group
     * @param       $associateUserGroupRequest
     * @return GroupsHasUser|Model
     */
    public function associateUser( Group $group, $associateUserGroupRequest )
    {

        $this->deleteGroupAssociateUser( $group, $associateUserGroupRequest );

        return $this->groupRepository->saveGroupHasUser( $group, $associateUserGroupRequest->id_user );

    }

    /**
     * @param Group $group
     * @param       $associateUserGroupRequest
     * @return mixed
     */
    public function deleteGroupAssociateUser( Group $group, $associateUserGroupRequest )
    {

        return $this->groupRepository->deleteUserAssociateToGroup( $associateUserGroupRequest->id_user, $group );

    }

    /**
     * @param $storeNoteRequests
     * @param $note
     */
    public function saveGroupHasNote( $storeNoteRequests, $note ): void
    {

        $group = $this->groupRepository->findOneGroup( $storeNoteRequests->id_group )->first();

        if ( !empty( $group ) ) {
            $group->notes()->attach( [ $note->id_note ] );
        }

    }

    /**
     * @param      $storeNoteRequests
     * @param Note $note
     */
    public function sendNotification( $storeNoteRequests, Note $note )
    {

        $group = $this->groupRepository->findOneGroup( $storeNoteRequests->id_group )
            ->with( 'users.accessToken' )
            ->first()
        ;
        if ( !empty( $group ) ) {
            $users = $group->getRelation( 'users' );
            $users->map( $this->mapSendNotificationNoteCreateTransform( $note ) );
        }

    }

    /**
     * @return callable
     */
    private function mapSendNotificationNoteCreateTransform( $note ): callable
    {

        return function ( $item, $key ) use ( $note ) {

            $user        = $item;
            $accessToken = $user->getRelation( 'accessToken' )->first();
            if ( !empty( $accessToken ) ) {
                $typeNotification = $accessToken->revoked === 0 ? [ 'broadcast' ] : [ 'mail' ];
                $item->notify( new NoteStoreNotification( $typeNotification, $note ) );
            }

            return $item;
        };
    }

}
