<?php
namespace App\Core\Groups\Models;

use App\Core\Notes\Models\Note;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Core\Groups\Models\GroupsHasNote
 *
 * @property int $id_note id note
 * @property timestamp $created_at created at
 * @property timestamp $updated_at updated at
 * @property IdNote $note belongsTo
 * @property int $id_group relation groups
 * @property-read \App\Core\Notes\Models\Note $notes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasNote newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasNote newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasNote query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasNote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasNote whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasNote whereIdNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasNote whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasNote whereDeletedAt($value)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\GroupsHasNote onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\GroupsHasNote withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\GroupsHasNote withoutTrashed()
 * @property-read \App\Core\Notes\Models\Note $notes
 */
class GroupsHasNote extends Model
{

    use SoftDeletes;
    /**
    * Database table name
    */
    protected $table = 'groups_has_notes';

    /**
    * Mass assignable columns
    */
    protected $fillable=['id_note'];

    /**
    * Date time columns.
    */
    protected $dates=[];

    /**
    * idNote
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function notes()
    {
        return $this->belongsTo(Note::class,'id_note');
    }




}
