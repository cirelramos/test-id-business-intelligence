<?php

namespace App\Core\Groups\Repositories\Usages;

use App\Core\Base\Repositories\Usages\BaseRepository;
use App\Core\Groups\Models\Group;
use App\Core\Groups\Models\GroupsHasUser;
use App\Core\Groups\Repositories\Interfaces\GroupRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GroupRepository
 * @package App\Core\Groups\Repositories\Usages
 */
class GroupRepository extends BaseRepository implements GroupRepositoryInterface
{

    /**
     * @var Group
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Group $model
     */
    public function __construct( Group $model )
    {

        $this->model = $model;
        parent::__construct( $model );
    }

    /**
     * @param $idGroup
     * @return Builder
     */
    public function findOneGroup( $idGroup ): Builder
    {

        return $this->getModel()::where( 'id_group', $idGroup );
    }

    /**
     * @return Group
     */
    public function getModel(): Group
    {

        return $this->model;
    }

    /**
     * @param       $idUser
     * @param Group $group
     * @return mixed
     */
    public function deleteUserAssociateToGroup( $idUser, Group $group )
    {

        return $this->getGroupsHasUser()::where( 'id_user', $idUser )->where( 'id_group', $group->id_group )->delete();

    }

    /**
     * @return GroupsHasUser
     */
    public function getGroupsHasUser(): GroupsHasUser
    {

        return new GroupsHasUser();

    }

    /**
     * @param Group $group
     * @param       $idUser
     * @return GroupsHasUser|Model
     */
    public function saveGroupHasUser( Group $group, $idUser )
    {

        return $this->getGroupsHasUser()::create( [ 'id_group' => $group->id_group, 'id_user' => $idUser ] );
    }

}
