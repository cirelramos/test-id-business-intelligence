<?php

namespace App\Console;

use App\Console\Commands\SendNotificationStoreNoteEveryWeekCommand;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SendNotificationStoreNoteEveryWeekCommand::class
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule( Schedule $schedule )
    {

        $dateTracker = Carbon::now( 'America/Lima' )->format( 'Y-m-d H:i:s' );

        $schedule->command( 'command:notification_store_note_every_week' )
//            ->everyMinute()
            ->weekly()
            ->sundays()
            ->at('13:20')
            ->timezone('America/Lima')
            ->name( $dateTracker . ' | Notification store note every week.' )
            ->withoutOverlapping()
        ;
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {

        $this->load( __DIR__ . '/Commands' );

        require base_path( 'routes/console.php' );
    }
}
