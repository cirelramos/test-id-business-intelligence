<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGroupsHasNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups_has_notes', function (Blueprint $table) {
            $table->integer('id_group')->unsigned()->comment('relation groups');
            $table->integer('id_note')->unsigned()->comment('relation notes');
            $table->foreign('id_group')
                ->references('id_group')
                ->on('groups')
                ->onDelete('cascade')
            ;
            $table->foreign('id_note')
                ->references('id_note')
                ->on('notes')
                ->onDelete('cascade')
            ;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups_has_notes');
    }
}
