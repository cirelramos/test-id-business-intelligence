<?php

use App\Core\Groups\Models\Group;
use Illuminate\Database\Seeder;

/**
 * Class GroupsTableSeeder
 */
class GroupsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {

        DB::statement( 'SET FOREIGN_KEY_CHECKS=0;' );
        Group::truncate();
        Group::flushEventListeners();

        $groups = [];
        for ( $i = 1; $i <= 6; $i++ ) {
            $groups[] = [ 'name' => 'Group ' . $i ];
        }
        Group::insert( $groups );
        DB::statement( 'SET FOREIGN_KEY_CHECKS=1;' );
    }
}
