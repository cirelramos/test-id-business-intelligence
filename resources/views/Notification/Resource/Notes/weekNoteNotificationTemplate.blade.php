@extends('Notification.Resource.layout.app')

@section('content')

    <p>
        Notas creadas esta semana
    </p>

    <table>
        @foreach($notes as $note)
            <tr>
                <td>
                    <p>
                        <b>NOTA:</b> {{$note->name}}
                    </p>
                    <p>
                        <b>DESCRIPCIÓN:</b> {{$note->description}}
                    </p>
                </td>
                <td>
                    @foreach($note->images as $image)
                        <img width="250" style="margin-top: 25px" src="{{$image->path}}" alt="">
                    @endforeach
                </td>
            </tr>
        @endforeach
    </table>

@endsection
